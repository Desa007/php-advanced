<?php


namespace App\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


final class Group extends Model
{
    use SoftDeletes;

    public function course()
    {
        return$this->belongsTo(Course::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class);
    }
}
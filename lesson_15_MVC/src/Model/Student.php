<?php


namespace App\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

final class Student extends Model
{
    use SoftDeletes;

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }
}
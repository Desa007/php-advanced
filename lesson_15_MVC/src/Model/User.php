<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

final class User extends Model
{
    public function phone(){
        return $this->hasOne(\App\Model\Phone::class);
    }

    public function posts(){
        return $this->hasMany(\App\Model\Post::class);
    }
}

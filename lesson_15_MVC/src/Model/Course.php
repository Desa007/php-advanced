<?php


namespace App\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

final class Course extends Model
{
    use SoftDeletes;

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
<?php


namespace App\Model;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

final class Lesson extends Model
{
    use SoftDeletes;
}
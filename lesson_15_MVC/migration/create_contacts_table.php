<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('contacts', function ($table){
    $table->bigIncrements('id');
    $table->string('phone', 100);
    $table->string('address', 100);
    $table->string('work_hours', 100);
    $table->string('facebook', 100);
    $table->string('twitter', 100);
    $table->string('gmail', 100);
    $table->timestamps();
});

$data = [
    ['phone' => '8 800 269 8469', 'address' => '253 Adams Ave, Iowa', 'work_hours' => 'Mon - Sat 8am - 6pm', 'facebook' => 'facebook.com', 'twitter' => 'twitter.com', 'gmail' => 'gmail.com'],
];

foreach ($data as $contacts) {
    $model = new \App\Model\Contacts();
    $model->phone = $contacts['phone'];
    $model->address = $contacts['address'];
    $model->work_hours = $contacts['work_hours'];
    $model->facebook = $contacts['facebook'];
    $model->twitter = $contacts['twitter'];
    $model->gmail = $contacts['gmail'];
    $model->save();
}
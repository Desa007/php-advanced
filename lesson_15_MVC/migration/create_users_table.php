<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('users', function ($table){
    $table->bigIncrements('id');
    $table->string('email', 100);
    $table->string('password', 100);
    $table->timestamps();
});

$data = [
    ['email' => 'devops@travellizy.com', 'password' => password_hash('password', PASSWORD_BCRYPT)],

];

foreach ($data as $user) {
    $model = new \App\Model\User();
    $model->email = $user['email'];
    $model->password = $user['password'];
    $model->save();
}
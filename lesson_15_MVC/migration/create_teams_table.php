<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('teams', function ($table) {
    $table->bigIncrements('id');
    $table->string('image', 100);
    $table->string('position', 100);
    $table->string('name', 100);
    $table->string('surname', 100);
    $table->string('description', 100);
    $table->string('facebook', 100);
    $table->string('twitter', 100);
    $table->string('gmail', 100);
    $table->timestamps();
});

$data = [
    [
        'image' => 'images/team/01.jpg',
        'position' => 'DIRECTOR',
        'name' => 'Theresa',
        'surname' => 'Green',
        'description' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'facebook.com',
        'twitter' => 'twitter.com',
        'gmail' => 'gmail.com',
    ],
    [
        'image' => 'images/team/02.jpg',
        'position' => 'CAT GROOMER',
        'name' => 'Rosie',
        'surname' => 'White',
        'description' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'facebook.com',
        'twitter' => 'twitter.com',
        'gmail' => 'gmail.com',
    ],
    [
        'image' => 'images/team/03.jpg',
        'position' => 'DOG GROOMER',
        'name' => 'Estelle',
        'surname' => 'Marsh',
        'description' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'facebook.com',
        'twitter' => 'twitter.com',
        'gmail' => 'gmail.com',
    ],
    [
        'image' => 'images/team/04.jpg',
        'position' => 'RABBIT GROOMER',
        'name' => 'Alberta',
        'surname' => 'Rogers',
        'description' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'facebook.com',
        'twitter' => 'twitter.com',
        'gmail' => 'gmail.com',
    ],
    [
        'image' => 'images/team/05.jpg',
        'position' => 'CAT GROOMER',
        'name' => 'Bernard',
        'surname' => 'Lambert',
        'description' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'facebook.com',
        'twitter' => 'twitter.com',
        'gmail' => 'gmail.com',
    ],
    [
        'image' => 'images/team/06.jpg',
        'position' => 'RABBIT GROOMER',
        'name' => 'Frank',
        'surname' => 'Stephens',
        'description' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'facebook.com',
        'twitter' => 'twitter.com',
        'gmail' => 'gmail.com',
    ],

];

foreach ($data as $team) {
    $model = new \App\Model\Team();
    $model->image = $team['image'];
    $model->position = $team['position'];
    $model->name = $team['name'];
    $model->surname = $team['surname'];
    $model->description = $team['description'];
    $model->facebook = $team['facebook'];
    $model->twitter = $team['twitter'];
    $model->gmail = $team['gmail'];

    $model->save();
}
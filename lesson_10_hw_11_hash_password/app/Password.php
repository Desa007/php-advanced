<?php


namespace App;


use App\Algorithm\AlgorithmInterface;

class Password implements PasswordInterface
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function hash(AlgorithmInterface $algorithm): string
    {
        return password_hash();
    }

    public function verify(string $hash): bool
    {
        // TODO: Implement verify() method.
    }

    public static function needsRehash($hash, AlgorithmInterface $algorithm): bool
    {
        // TODO: Implement needsRehash() method.
    }
}
<?php
declare(strict_types=1);

namespace App\Model;

class ShawarmaCalculator
{
    public function add(ShawarmaInterface $data, array $order): array
    {
        if (array_key_exists($data->getTitle(), $order)) {
            ++$order[$data->getTitle()];
        } else {
            $order[$data->getTitle()] = 1;
            $order['price'][$data->getTitle()] = $data->getCost();
            $order['ingredients'][] = $data->getIngredients();
        }
        return $order;

    }

    public function ingredients(ShawarmaInterface $data): array
    {
        return $data->getIngredients();
    }

    public function price(array $order): float
    {
        $totalPrice = 0;
        foreach ($order as $key => $value) {
            if ($key === 'price' || $key === 'ingredients') {
                continue;
            }
            $price = $order[$key] * $order['price'][$key];
            $totalPrice += $price;
        }
        return $totalPrice;
    }
}
<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use UAParser\Parser;

class ProcessParser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ip;
    protected $userAgent;
    protected $code;

    /**
     * ProcessParser constructor.
     * @param $ip
     * @param $userAgent
     * @param $code
     */
    public function __construct($ip, $userAgent, $code)
    {
        $this->ip = $ip;
        $this->userAgent = $userAgent;
        $this->code = $code;
    }

    /**
     * @param \App\AdapterInterface $adapter
     * @throws \UAParser\Exception\FileNotFoundException
     */
    public function handle()
    {
        $link = \App\Link::where('short_code', $this->code)->get()->first();

//        $adapter->parse($this->ip);
//        $city = $adapter->getCityName();
//        $countryCode = $adapter->getCountryCode();

        // ua-parser uap-php
        $reader = Parser::create();

        $result = $reader->parse($this->userAgent);

        $browser = $result->ua->toString();
        $data[] = $browser;
        $engine = $result->os->toString();
        $data[] = $engine;
        $os = $result->os->toString();
        $data[] = $os;
        $device = $result->device->toString();
        $data[] = $device;


        // MaxMind
    $reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');
    try {
        $record = $reader->city(request()->ip());
    } catch (\GeoIp2\Exception\AddressNotFoundException $exception) {
        $record = $reader->city(env('DEFAULT_IP_ADDR'));
    } finally {
        $city = $record->city->name;
        $countryCode = $record->country->isoCode;
    }

        // Add data into table Statistic
        $statistics = new \App\Statistic();
        // uuid generate
        $statistics->id = \Ramsey\Uuid\Uuid::uuid4()->toString();
        $statistics->link_id = $link->id;
        $statistics->ip = request()->ip();
        $statistics->user_agent = $this->userAgent;
        $statistics->country_code = $countryCode;
        $statistics->city_name = $city;
        $statistics->browser = $browser;
        $statistics->engine = $engine;
        $statistics->os = $os;
        $statistics->device = $device;
        $statistics->save();

    }
}

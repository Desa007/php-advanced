<?php

namespace App\Jobs;

use App\Http\Controllers\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        echo $this->message->getMessage() . PHP_EOL;

        file_get_contents('https://api.telegram.org/bot1063364267:AAFDuEEaoXZiWCgJoF40zaXWpguhqFYTags/sendMessage?chat_id=576760681&text=' . $this->message->getMessage());
    }
}

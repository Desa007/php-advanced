<?php


namespace App\Http\Controllers;


class MainController
{
    public function index() {

        $googleUrl = 'https://accounts.google.com/o/oauth2/v2/auth?';
        $parametrs = [
            'client_id' => env('OAUTH_GOOGLE_CLIENT_ID'),
            'response_type' => 'code',
            'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
            'redirect_uri' => env('OAUTH_GOOGLE_REDIRECT_URI'),
        ];
        $googleUrl .= http_build_query($parametrs);

        $githubUrl = 'https://github.com/login/oauth/authorize?';
        $parametrs = [
            'client_id' => env('OAUTH_GITHUB_CLIENT_ID'),
            'redirect_uri' => env('OAUTH_GITHUB_REDIRECT_URI'),
            'scope' => 'read:user,user:email',
        ];
        $githubUrl .= http_build_query($parametrs);

        $data['google'] = $googleUrl;
        $data['github'] = $githubUrl;

        return view('welcome', $data);
    }

}

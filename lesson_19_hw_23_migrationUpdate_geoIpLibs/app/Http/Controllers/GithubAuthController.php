<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class GithubAuthController
{
    public function callback(\Illuminate\Http\Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://github.com/login/oauth/access_token', [
            'form_params' => [
                'client_id' => env('OAUTH_GITHUB_CLIENT_ID'),
                'client_secret' => env('OAUTH_GITHUB_CLIENT_SECRET'),
                'code' => $request->get('code'),
                'redirect_uri' => env('OAUTH_GITHUB_REDIRECT_URI'),
            ]
        ]);

        $response = $response->getBody()->getContents();
        $result  = [];
        parse_str($response, $result);

        $response = $client->request('GET', 'https://api.github.com/user', [
            'headers' => [
                'Authorization' => 'token ' . $result['access_token'],
            ]
        ]);
        $userInfo = json_decode($response->getBody()->getContents(), true);

        $response = $client->request('GET', 'https://api.github.com/user/emails', [
            'headers' => [
                'Authorization' => 'token ' . $result['access_token'],
            ]
        ]);
        $userEmails = json_decode($response->getBody()->getContents(), true);

        $email = null;
        foreach ($userEmails as $userEmail){
            if ($userEmail['primary'] === true){
                $email = $userEmail['email'];
                break;
            }
        }

        $user = \App\User::where('email', '=', $email)->get()->first();
        if (!$user) {
            $user = new \App\User();
            if($user->name === null) {
                $user->name = 'Default_user_name';
            }else{
                $user->name = $userInfo['name'];
            }
            $user->email = $email;
            $user->email_verified_at = now();
            $user->password = Hash::make(Illuminate\Support\Str::random(10));
            $user->remember_token = Illuminate\Support\Str::random(10);
            $user->save();
        }

        Auth::login($user, true);
        return redirect()->route('home');

    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Message;
use App\Jobs\ProcessMessage;
use App\Jobs\ProcessParser;
use Hillel\Library\IpApiAdapter;
use Hillel\Library\MaxMindAdapter;
use Illuminate\Support\Facades\Route;
//use UAParser\Parser;
use \Dejurin\GoogleTranslateForFree;

App::singleton(\App\AdapterInterface::class, function () {
    $reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');

//    return new \App\MaxMindAdapter($reader);
//    return new \App\IpapiAdapter();

    // with composer
//    return new MaxMindAdapter($reader);
    return new IpApiAdapter($reader);
});

App::singleton(\App\AdapterUserAgentInterface::class, function () {
    //-1-
    return new \App\WhichBrowserAdapter();
    //-2-
//    return new \App\UserAgentParserAdapter();
});

Route::get('/', '\App\Http\Controllers\MainController@index')->name('home');

Route::get('/callback', '\App\Http\Controllers\GithubAuthController@callback')->name('callback');
Route::get('/google-callback', '\App\Http\Controllers\GoogleAuthController@callback')->name('google-callback');

Route::get('/queue/{message}', function ($message){
    $msg = new Message($message);
    ProcessMessage::dispatch($msg)->onQueue('second');
});

Route::get('/translate', function (){
    $source = 'en';
    $target = 'ru';
    $attempts = 5;
    $text = 'Why my order is cancled';

    $tr = new GoogleTranslateForFree();
    $result = $tr->translate($source, $target, $text, $attempts);

    dd($result);
});
//Route::resources([
//    'users' => '\App\Http\Controllers\UserController',
//]);

Route::prefix('/admin')->middleware('auth')->group(function () {
    Route::get('/users', '\App\Http\Controllers\UserController@index')->name('users.index');
    Route::get('/users/create', '\App\Http\Controllers\UserController@create')->name('users.create');
    Route::post('/users/create', '\App\Http\Controllers\UserController@store')->name('users.store');
    Route::get('/users/{user}', '\App\Http\Controllers\UserController@show')->name('users.show');
    Route::get('/users/{user}/edit', '\App\Http\Controllers\UserController@edit')->name('users.edit');
    Route::match(['put', 'patch'], '/users/{user}/edit', '\App\Http\Controllers\UserController@update')->name('users.update');
    Route::delete('/users/{user}', '\App\Http\Controllers\UserController@destroy')->name('users.destroy');
});

Route::middleware('auth')->group(function() {
    Route::get('/wall', '\App\Http\Controllers\WallController@index')->name('wall.index');

    Route::get('/create-post', '\App\Http\Controllers\WallController@create')->name('post.create');
    Route::post('/create-post', '\App\Http\Controllers\WallController@store')->name('post.store');
});

Route::get('/sign-up', '\App\Http\Controllers\SignUpController@index')->name('sign-up');
Route::post('/sign-up', '\App\Http\Controllers\SignUpController@handle')->name('handle-sign-up');

Route::get('/sign-in', '\App\Http\Controllers\SignInController@index')->name('login');
Route::post('/sign-in', '\App\Http\Controllers\SignInController@handle')->name('handle-sign-in');

Route::get('/logout', '\App\Http\Controllers\SignUpController@logout')
    ->name('logout')
    ->middleware('auth');

//Route::get('/r/{code}', function ($code, \App\AdapterInterface $adapter, \App\AdapterUserAgentInterface $userAgent) {
//Route::get('/r/{code}', function ($code, \App\AdapterInterface $adapter, \App\AdapterUserAgentInterface $userAgent) {
Route::get('/r/{code}', static function ($code) {

//    cache()->forget('get-short-links');
//    exit;

//    $link = \App\Link::where('short_code', $code)->get()->first();
//    $source_link = $link->source_link;

    $links = \Illuminate\Support\Facades\Cache::remember('get-short-links', '86400', function () {
        return \App\Link::all();
    });

//    $sourceCode = '';
//    foreach ($links as $link) {
//        if ($link->short_code === $code) {
//            $sourceCode = $link->source_link;
//        }
//    };

    $sourceLink = $links->filter(function (App\Link $link) use ($code) {
        return $link->short_code === $code;
    })->first();

    $source_link = $sourceLink->source_link;

    ProcessParser::dispatch(request()->ip(), $_SERVER['HTTP_USER_AGENT'], $code)->onQueue('userAgent');

//    $link = \App\Link::where('short_code', $code)->value('source_link');


//    $adapter->parse(request()->ip());
//    $city = $adapter->getCityName();
//    $countryCode = $adapter->getCountryCode();

    // MaxMind
//    $reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');
//    try {
//        $record = $reader->city(request()->ip());
//    } catch (\GeoIp2\Exception\AddressNotFoundException $exception) {
//        $record = $reader->city(env('DEFAULT_IP_ADDR'));
//    } finally {
//        $city = $record->city->name;
//        $countryCode = $record->country->isoCode;
//    }



    // IP-API
//    $result = file_get_contents('http://ip-api.com/json/'. request()->ip());
//    $data = json_decode($result, true);
//
//    if($data['status'] =='fail'){
//        $result = file_get_contents('http://ip-api.com/json/'. env('DEFAULT_IP_ADDR'));
//        $data = json_decode($result, true);
//
//        $city = $data['city'];
//        $countryCode = $data['countryCode'];
//    }

    // ua-parser uap-php
//    $reader = Parser::create();
//    $result = $reader->parse($_SERVER['HTTP_USER_AGENT']);
//
//    $browser = $result->ua->toString();
//    $data[] = $browser;
//    $engine = $result->os->toString();
//    $data[] = $engine;
//    $os = $result->os->toString();
//    $data[] = $os;
//    $device = $result->device->toString();
//    $data[] = $device;


    //yzalis/ua-parser
    // create a new UAParser instance
//    $uaParser = new \UAParser\UAParser();

// ...or optionally load a custom regexes.yml file of your choice
// $uaParser = new \UAParser\UAParser(__DIR__.'/../../custom_regexes.yml');

// parse a user agent string an get the result
//    $result =  $uaParser->parse('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0.1');


    // Add data into table Statistic
//    $statistics = new \App\Statistic();
//    // uuid generate
//    $statistics->id = \Ramsey\Uuid\Uuid::uuid4()->toString();
//    $statistics->link_id = $link->id;
//    $statistics->ip = request()->ip();
//    $statistics->user_agent = request()->userAgent();
//    $statistics->country_code = $countryCode;
//    $statistics->city_name = $city;
//    $statistics->browser = $browser;
//    $statistics->engine = $engine;
//    $statistics->os = $os;
//    $statistics->device = $device;
//    $statistics->save();

//    dd($statistics);

    if($source_link == NULL) {
        return redirect('/');
    } else {
        return redirect($source_link);
    }
});
Route::get('/cache', function (){
//    cache()->forget('get-all-users');
//    exit;

//    $users = \Illuminate\Support\Facades\Cache::remember('get-all-users', 86400, function (){
//        return \App\User::all();
//    });
//    return view('sign-in');
    return view('sign-in');
});

@extends('admin.layout')

@section('content')
    <form method="post" action="{{ route('users.store') }}">
        @csrf
        <legend>Create User</legend>

        <div class="form-group">
            <label class="col-md-4" for="name">Name</label>
            @if ($errors->has('name'))
                <ul class="alert alert-danger col-md-4">
                    @foreach($errors->get('name') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="col-md-4">
                <input id="textinput" type="text" class="form-control" name="name" value="{{ old('name') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4" for="email">Email</label>
            @if ($errors->has('email'))
                <ul class="alert alert-danger col-md-4">
                    @foreach($errors->get('email') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="col-md-4">
                <input type="email" class="form-control" id="textinput" name="email" value="{{ old('email') }}">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4" for="password">Password</label>
            @if ($errors->has('password'))
                <ul class="alert alert-danger col-md-4">
                    @foreach($errors->get('password') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="col-md-4">
                <input type="password" class="form-control" id="password" name="password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4" for="password_confirmation">Password Confirmation</label>
            @if ($errors->has('password_confirmation'))
                <ul class="alert alert-danger col-md-4">
                    @foreach($errors->get('password_confirmation') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="col-md-4">
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
            </div>
        </div>

        <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Create</button>
    </form>
@endsection

<?php
declare(strict_types=1);

namespace App\Services;


class AutoloadPsr4
{
    protected $prefixes;

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }
    public function addNamespase(string $prefix, string $base_dir)
    {
        // normalize namespace prefix
        $prefix = trim($prefix, '\\') . '\\';

        // normalize the base directory with a trailing separator
        $base_dir = rtrim($base_dir, DIRECTORY_SEPARATOR) . '/';

        // initialize the namespace prefix array
        if (isset($this->prefixes[$prefix]) === false) {
            $this->prefixes[$prefix] = array();
        }

        array_push($this->prefixes[$prefix], $base_dir);
    }
}
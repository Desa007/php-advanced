<?php

declare(strict_types=1);

/**
 * Class Money
 */
class Money
{
    private $amount;
    private $currency;

    /**
     * Money constructor.
     * @param float $value
     * @param Currency $currency
     */
    public function __construct(float $value, Currency $currency)
    {
        if ($value < 0) {
            throw new InvalidArgumentException('Your amount must be 0 or higher');
        }
        $this->amount = $value;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function equals(self $money): bool
    {
        return (string)$this->currency === (string)$money->currency;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->amount . ' ' . (string)$this->currency;
    }

    public function add(self $money): float
    {
        return $this->amount + $money->getAmount();
    }
}
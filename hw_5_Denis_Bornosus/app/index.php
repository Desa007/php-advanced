<?php
declare(strict_types=1);
//phpinfo();

require_once 'vendor/autoload.php';

//require_once 'Currency.php';
//require_once 'Money.php';

use \App\Services\Currency;
use \App\Services\Money;

$currency1 = new Currency('USD');
$currency2 = new Currency('USD');
$money1 = new Money(2, $currency1);
$money2 = new Money(1, $currency2);

//echo (string)$currency1;

try {
//    $currency2 = new Currency('USD');
//    echo $currency1->equals($currency2) ? 'Your currencies are equal' : 'Your currencies NOT equal'; //сравнение вводимых валют
    if ($money1->equals($money2) === false){
        throw new Exception('Your currencies NOT equal');
    }
    echo 'Your currencies are equal';

//    echo $money1->equals($money2) ? 'Your currencies are equal' : 'Your currencies NOT equal'; // сравнение по числу и валюте
    echo $money1->add($money2) . ' ' . $money1->getCurrency(); // суммирование валюты
} catch (Exception $exception){
    echo $exception->getMessage();
}

//$text = "These are a few words :) ...     ";
//var_dump($text);
//
//print "\n";
//
//$trimmed = trim($text, "T ") . '\\';
//var_dump($trimmed);
<?php

function email_validator(string $value) : bool
{
    $result = false;

    if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
        $result = true;
    }

    return $result;
}

function not_empty_validator($value) : bool
{
    $result = false;

    if (!empty($value)) {
        $result = true;
    }

    return $result;
}

function url_validator(string $value) : bool
{
    $result = false;
    $schema = parse_url($value, PHP_URL_SCHEME);

    if ($schema === 'http' || $schema === 'https') {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            $result = true;
        }
    }
    return $result;
}

function number_validator($value): bool
{
    return is_numeric($value);
}

function in_validator($value, array $list): bool
{
    return in_array($value, $list, true);
}
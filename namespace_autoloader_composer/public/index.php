<?php

require_once '../vendor/autoload.php';

use \App\Ivanov\Currency as IvanovCurrency;
use \App\Petrov\Currency as PetrovCurrency;

$ivanovCurrency = new IvanovCurrency();
$petrovCurrency = new PetrovCurrency();

$model = new \Core\Model\User();

$qwe = new \Hillel\PHP\Hydrator\User\Comment();
<?php
declare(strict_types=1);

require_once '../vendor/autoload.php';

use App\Services\Finder;

$obj = new Finder;

echo Finder::findByEmailAndByStatus('devops@travellizy.com', 'in_process');
echo '<br>';
//echo Finder::findById(123);
echo '<br>';
//echo Finder::findBetweenCreatedAt($startDate, $endDate);
echo '<br>';
//echo Finder::findBetweenCreatedAtAndByStatus('2020-01-28', '2020-01-29', 'in_progress');

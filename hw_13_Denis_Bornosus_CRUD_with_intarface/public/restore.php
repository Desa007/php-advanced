<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

//restore
$category = App\Model\Category::withTrashed()
    ->where('id', $_GET['id']);
$category->restore();

// show delete models
$categories = App\Model\Category::onlyTrashed()->get()

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= 'Restore Models' ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="container">
        <h1>Deleted Models</h1>
        <table class="table">
            <thead>
            <th>Id</th>
            <th>Name</th>
            <th>Created_at</th>
            <th>Updated_at</th>
            <th>Deleted_at</th>
            </thead>
            <?php foreach ($categories as $category) : ?>
                <tbody>
                <td><?= $category->id ?> </td>
                <td><?= $category->name ?> </td>
                <td><?= $category->created_at ?> </td>
                <td><?= $category->updated_at ?> </td>
                <td><?= $category->deleted_at ?> </td>
                <td>
                    <a class="btn btn-primary" href="restore.php?id=<?= $category->id ?>">Restore</a>
                </td>
                </tbody>
            <?php endforeach ?>
        </table>
    </div>
    <p><a class="btn btn-primary" href="/index.php">Back</a></p>
</div>
</body>
</html>

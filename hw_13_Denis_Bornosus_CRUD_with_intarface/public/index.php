<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

$categories = App\Model\Category::all();

// for view deleted records
//$categories = App\Model\Category::withTrashed()->get();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Models</title>
</head>
<body>
<div class="container">
    <h1>Model "Categories"</h1>
    <a class="btn btn-primary" href="create.php">Create new Model</a>
    <a class="btn btn-primary" href="restore.php">Show deleted Models</a>
    <table class="table">
        <thead>
        <th>Id</th>
        <th>Name</th>
        <th>Created_at</th>
        <th>Updated_at</th>
        <th>Modify</th>
<!--        <th>Deleted_at</th>-->
        </thead>
        <?php foreach ($categories as $category) : ?>
            <tbody>
            <td><?= $category->id ?> </td>
            <td><?= $category->name ?> </td>
            <td><?= $category->created_at ?> </td>
            <td><?= $category->updated_at ?> </td>
            <td>
                <a class="btn btn-primary" href="update.php?id=<?=$category->id?>">Update</a>
                <a class="btn btn-primary" href="delete.php?id=<?=$category->id?>">Delete</a>
            </td>
<!--            <td>--><?//= $category->deleted_at ?><!-- </td>-->
            </tbody>
        <?php endforeach ?>
    </table>
</div>
</body>
</html>

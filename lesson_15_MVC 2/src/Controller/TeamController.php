<?php

namespace App\Controller;

class TeamController
{
    public function __invoke()
    {
        $teams = \App\Model\Team::all();

        return view('team', ['teams' => $teams]);
    }
}

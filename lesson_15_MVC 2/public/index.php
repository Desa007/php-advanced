<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';
require_once '../config/blade.php';

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);
$router = new \Illuminate\Routing\Router($dispatcher, $container);

function view($view, array $data = [])
{
    global $blade;

    return $blade->make($view, $data)->render();
}

function router()
{
    global $router;
    return $router;
}

$router->get('/', \App\Controller\HomeController::class)->name('home');
$router->get('/blog', \App\Controller\BlogController::class)->name('blog');
$router->get('/services', \App\Controller\ServicesController::class)->name('services');
$router->get('/team', \App\Controller\TeamController::class)->name('team');
$router->get('/contact-us', \App\Controller\ContactController::class)->name('contacts');


$router->get('/one-to-one', function (){
    $user = \App\Model\User::find(1);
    var_dump($user->phone->phone);
});

$router->get('/one-to-many', function (){
    $user = \App\Model\User::find(1);
    var_dump($user->posts);
});

$router->get('/one-to-many-inverse', function (){
    $post = \App\Model\Post::find(1);
    var_dump($post->user);
});

$router->get('/many-to-many', function(){
//    $post = \App\Model\Post::find(2);
//    var_dump($post->tags);

//    $tag = \App\Model\Tag::find(1);
//    var_dump($tag->posts);
});


$request = \Illuminate\Http\Request::createFromGlobals();

$response = $router->dispatch($request);

echo $response->getContent();
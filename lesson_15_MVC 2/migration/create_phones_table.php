<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('phones', function ($table){
    $table->bigIncrements('id');
    $table->string('phone', 100);
    $table->unsignedBigInteger('user_id');
    $table->timestamps();

    $table->foreign('user_id')->references('id')->on('users');
});

$data = [
    ['id' => '1', 'user_id' => 1, 'phone' => '+38000000000'],

];

foreach ($data as $phone) {
    $model = new \App\Model\Phone();
    $model->id = $phone['id'];
    $model->phone = $phone['phone'];
    $model->user_id = $phone['user_id'];
    $model->save();
}
<?php

namespace App\Model;

use mysql_xdevapi\Exception;

final class User extends Model
{
    protected $id;
    protected $email;
    protected $pass;

    /**
     * User constructor.
     * @param $id
     * @param $email
     */
    public function __construct($id = null, $email, $pass)
    {
        $this->id = $id;
        $this->email = $email;
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass): void
    {
        $this->pass = $pass;
    }

    public static function find($id)
    {
        $data =  parent::find($id);
        return new self($data['id'], $data['email'], $data['pass']);
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

}
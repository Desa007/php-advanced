<?php
declare(strict_types=1);

require_once '../vendor/autoload.php';

use App\Model\User;

// select
//$user = User::find(2);
//var_dump($user);

// delete
//$user = User::find(1);
//$user->delete();

//create
//$user = new User(null,'dev4444444@mail.ru', 'qwerty');
//$user->create();

//update
$user = User::find(1);
$user->setEmail('blabla');
$user->setPass(123123123);
$user->update();
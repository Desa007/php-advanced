<?php

namespace App\Services;

use Exception;
use function Sodium\crypto_box_publickey_from_secretkey;

class Finder
{
    private const SQL_FIND = 'SELECT * FROM table WHERE ';

    public static function __callStatic($name, $arguments)
    {
        echo '<pre>';

        if (mb_strripos($name, 'find') === 0) {
            $query = ltrim($name, 'find');
            print_r(self::SQL_FIND . self::buildWhere(self::prepareParams($query), $arguments));
            die();
        }

        echo 'Not Valid Method';
        die();
    }

    private static function prepareParams(string $input): array
    {
        $data = explode('And', $input);

        return array_map(static function (string $arg): string {
            if (strpos($arg, 'By') !== false) {
                $argWithoutBy = str_replace('By', '', $arg);
                return self::camelToSnake($argWithoutBy) . 'BY';
            }
            if (strpos($arg, 'Between') !== false) {
                $argWithoutBetween = str_replace('Between', '', $arg);
                return self::camelToSnake($argWithoutBetween) . 'BETWEEN';
            }
            if (strpos($arg, 'In') !== false) {
                $argWithoutIn = str_replace('In', '', $arg);
                return self::camelToSnake($argWithoutIn) . 'IN';
            }
            echo "Wrong parametrs";
            die();
        }, $data);
    }

    private static function camelToSnake(string $input): string
    {
        return mb_strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }


    private static function buildWhere(array $params, array $values): string
    {
        $count = count($params);
        $result = [];
        $j=0;
        for ($i = 0; $i < $count; $i++) {
            if (strpos($params[$i], 'BETWEEN') !== false) {
                $params[$i] = str_replace('BETWEEN', '', $params[$i]);

                if (isset($values[$j+1]) === false){
                    throw new Exception('Please check parametrs');
                }
                $result[] = sprintf('%s BETWEEN :%s AND :%s', $params[$i], $values[$j], $values[$j + 1]);
                $j+=2;

            }
            if (strpos($params[$i], 'IN') !== false) {
                $params[$i] = str_replace('IN', '', $params[$i]);
                if (isset($values[$j+1]) === false){
                    throw new Exception('Please check parametrs');
                }
                $result[] = sprintf(' AND %s IN (:%s, :%s)', $params[$i], $values[$j], $values[$j + 1]);
                $j+=2;
            }
            if (strpos($params[$i], 'BY') !== false) {
                $params[$i] = str_replace('BY', '', $params[$i]);
                if (isset($values[$j]) === false){
                    throw new Exception('Please check parametrs');
                }
                if ($i > 0) {
                    $result[] = sprintf(' AND %s=:%s', $params[$i], $values[$j]);
                    $j++;
                } else {
                    $result[] = sprintf('%s=:%s', $params[$i], $values[$j]);
                    $j++;
                }
            }
        }

        return implode($result);
    }

}

<?php

namespace App\Services;

final class CustomAutoloadPsr4
{
    protected $namespace = [];

    public function register(): void
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function addNameSpace($prefix, $dirPath)
    {
        // Delete "\" from incoming prefix and add "\" to the end
        $prefix = trim($prefix, '\\') . '\\';
        // Delete constant "DIRECTORY_SEPARATOR" witch = "/" from the end of text and add "/"
        $dirPath = rtrim($dirPath, DIRECTORY_SEPARATOR) . '/';
        // Check array with incoming namespaces and add new one
        if (isset($this->namespace[$prefix]) === false) {
            $this->namespace[$prefix] = [];
        }
        $this->namespace[$prefix][] = $dirPath;

    }

    public function loadClass($class)
    {
        // the current namespace prefix
        $prefix = $class;

        // while find \ in $prefix do
        while (false !== $pos = strrpos($prefix, '\\')) {

            // cat end part from \
            $prefix = substr($class, 0, $pos + 1);

            // the rest is class name
            $relative_class = substr($class, $pos + 1);

            // try to load a mapped file for the prefix and class name
            $mapped_file = $this->loadMappedFile($prefix, $relative_class);
            if ($mapped_file) {
                return $mapped_file;
            }

            // remove part from \
            $prefix = rtrim($prefix, '\\');
        }

        // never found a mapped file
        return false;
    }

    protected function loadMappedFile($prefix, $relative_class)
    {
        // are there any base directories for this namespace prefix?
        if (isset($this->namespace[$prefix]) === false) {
            return false;
        }

        // look through base directories for this namespace prefix
        foreach ($this->namespace[$prefix] as $base_dir) {

            // get full path to class file + .php
            $file = $base_dir
                . str_replace('\\', '/', $relative_class)
                . '.php';

            // if the mapped file exists, require it
            if ($this->requireFile($file)) {
                // yes, we're done
                return $file;
            }
        }

        // never found it
        return false;
    }

    protected function requireFile($file)
    {
        if (file_exists($file)) {
            require $file;
            return true;
        }
        return false;
    }
}

$namespace = new CustomAutoloadPsr4();

//$namespace->addNameSpace('travellizy\.abc\aaa\dcsdv','www/public/');
//$namespace->addNameSpace('travellizy2\.abc\aaa\dcsdv','www/public/2');
$namespace->register();

//echo "<br>";
//
//$namespace3 = new Test();
//$namespace3->getVar();
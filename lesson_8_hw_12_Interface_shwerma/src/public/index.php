<?php
declare(strict_types=1);

require_once '../vendor/autoload.php';

use App\Model\ShawarmaOdesskaya;
use App\Model\ShawarmaBeef;

$shava = new ShawarmaOdesskaya('Odesskaya', ['огурцы маринованные', 'картофель жареный', 'чесночный соус',
    'тандырный лаваш', 'маринованный лук с барбарисом и зеленью',
    'мясо куриное', 'салат коул слоу', 'корейская морковь'], 69);
$name = $shava->getTitle();
$ingredients = $shava->getIngredients();
$cost = $shava->getCost();

$shava2 = new ShawarmaBeef('Beef', ['чесночный соус', 'говяжий окорок', 'огурцы маринованные', 'маринованный лук с барбарисом и зеленью',
    'салат коул слоу', 'тандырный лаваш', 'помидоры свежие', 'хумус', 'соус тахин'], 75);
$name = $shava2->getTitle();
$ingredients = $shava2->getIngredients();
$cost = $shava2->getCost();

$shava3 = new \App\Model\ShawarmaMutton('Mutton', ['острый соус', 'огурцы маринованные', 'кинза', 'помидоры свежие',
    'маринованный лук с барбарисом и зеленью', 'мясо баранины', 'лаваш арабский'], 85);
$name = $shava3->getTitle();
$ingredients = $shava3->getIngredients();
$cost = $shava3->getCost();


$calc = new \App\Model\ShawarmaCalculator();
$order = [];

$order = $calc->add($shava, $order);
$order = $calc->add($shava2, $order);
$order = $calc->add($shava2, $order);
$order = $calc->add($shava2, $order);
$order = $calc->add($shava2, $order);
//$order = $calc->add($shava3, $order);
//$order = $calc->add($shava3, $order);

$total = $calc->price($order);

var_dump($order, $total);


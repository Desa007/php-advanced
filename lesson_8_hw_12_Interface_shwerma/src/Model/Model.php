<?php


namespace App\Model;


abstract class Model
{
//    public function __construct($data)
//    {
//        foreach ($data as $field => $value) {
//            $this->$field = $value;
//        }
//    }

    public static function find($id)
    {
        $table = self::pluralize('2', strtolower(end(explode('\\', static::class))));

        $pdo = new \PDO('mysql:host=db;dbname=test1', 'root', 'rootPass');

        $sql = 'SELECT * FROM ' . $table . ' WHERE id = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        if ($data === false) {
            throw new \Exception('No such record');
        }
        return $data;
    }

    public function delete()
    {
        $table = self::pluralize('2', strtolower(end(explode('\\', static::class))));

        $pdo = new \PDO('mysql:host=db;dbname=test1', 'root', 'rootPass');
        $sql = 'DELETE FROM ' . $table . ' WHERE id = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':id', $this->id);
        $stmt->execute();
    }

    public function create()
    {
        $table = self::pluralize('2', strtolower(end(explode('\\', static::class))));

        $pdo = new \PDO('mysql:host=db;dbname=test1', 'root', 'rootPass');
        $sql = 'INSERT INTO ' . $table . ' (' . implode(', ',
                array_keys(get_object_vars($this))) . ')' . ' VALUES' . ' (:' . implode(', :',
                array_keys(get_object_vars($this))) . ')';
        $stmt = $pdo->prepare($sql);
        foreach (get_object_vars($this) as $field => $value) {
            if ($field === 'id') {
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
                continue;
            }
            $stmt->bindValue(':' . $field, $value);
        }
        $stmt->execute();
    }

    public function update()
    {
        $table = self::pluralize('2', strtolower(end(explode('\\', static::class))));
        $pdo = new \PDO('mysql:host=db;dbname=test1', 'root', 'rootPass');

//        $sql = 'UPDATE ' . $table . ' SET email = :email WHERE id = :id';
//        var_dump(get_object_vars($this));
        foreach (get_object_vars($this) as $field => $value) {
            if ($field === 'id') {
                continue;
            }
            $updateValues[] = $field . ' = :' . $field;
        }
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $updateValues) . ' WHERE id = :id';
//        var_dump($sql);

        $stmt = $pdo->prepare($sql);
        foreach (get_object_vars($this) as $field => $value) {
            $stmt->bindValue(':' . $field, $value);
        }
        $stmt->execute();

    }

    protected static function pluralize($quantity, $singular, $plural = null)
    {
        if ($quantity == 1 || !strlen($singular)) {
            return $singular;
        }
        if ($plural !== null) {
            return $plural;
        }

        $last_letter = strtolower($singular[strlen($singular) - 1]);
        switch ($last_letter) {
            case 'y':
                return substr($singular, 0, -1) . 'ies';
            case 's':
                return $singular . 'es';
            default:
                return $singular . 's';
        }
    }
}
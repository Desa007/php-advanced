<?php
declare(strict_types=1);

namespace App\Model;

/**
 * Class ShawarmaBeef
 * @package App\Model
 */
class ShawarmaBeef implements ShawarmaInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $ingredients;

    /**
     * @var float
     */
    private $cost;

    /**
     * ShawarmaBeef constructor.
     * @param string $name
     * @param array $ingredients
     * @param float $cost
     */
    public function __construct(string $name, array $ingredients, float $cost)
    {
        $this->name = $name;
        $this->ingredients = $ingredients;
        $this->cost =$cost;

    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @return array
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }
}
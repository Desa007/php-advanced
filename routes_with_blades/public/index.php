<?php

require_once '../vendor/autoload.php';
require_once '../config/blade.php';

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);
$router = new \Illuminate\Routing\Router($dispatcher, $container);

//echo $blade->make('layouts.layout')->render();
//var_dump($blade);

//$router->get('/', function() use($blade){
//    echo $blade->make('layouts.layout')->render();
//});

$router->get('/', \App\Controller\HomeController::class);
$router->get('/blog', \App\Controller\BlogController::class);


$router->get('/services', \App\Controller\ServicesController::class);

$router->get('/team/{id}', function (){
    require_once 'html/team.html';
});

$router->get('/contact-us', function (){
    require_once 'html/contact.html';
});

$request = \Illuminate\Http\Request::createFromGlobals();
$router->dispatch($request);
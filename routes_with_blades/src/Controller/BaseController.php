<?php

namespace App\Controller;

class BaseController
{
    public function index()
    {
        require_once 'html/index.html';
    }

    public function blog()
    {
        require_once 'html/blog-full.html';
    }
}
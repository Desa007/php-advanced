<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class GitlabApiRequest
 * @package App\Http\Requests
 *
 * @property string $function
 */
class GitlabApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'function' => 'required',
                'repository' => 'required|numeric'
            ];
    }
    public function messages()
    {
        return [
            'repository.digits' => 'Не верный ID репозитория'
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GitlabApiRequest;
use Illuminate\Support\Facades\Http;

class GitlabApiController extends Controller
{
    public function getContainerRegistryList(GitlabApiRequest $request) {
        $projectId = $_POST['repository'];
        $url = "https://gitlab.com/api/v4/projects/".$projectId."/registry/repositories";

        $response = Http::withHeaders([
            'PRIVATE-TOKEN' => env('PRIVATE_TOKEN')
        ])->get($url)->json();

        $data['repository'] = $response;
//dd($data['repository']);
        return view('api', $data);
    }

    public function getContainerRegistryTag(int $id) {
        $url = "https://gitlab.com/api/v4/registry/repositories/".$id."?tags=true&tags_count=true";

        $response = Http::withHeaders([
            'PRIVATE-TOKEN' => env('PRIVATE_TOKEN')
        ])->get($url)->json();

        $data['repositoryTag'] = $response;
//dd($data['repositoryTag']);
        return view('tags', $data);
    }
}

<?php

use App\Http\Controllers\GitlabApiController;
use Illuminate\Support\Facades\Route;

//Route::get('/', function () {
//    return view('welcome');
//})->name('home');


Route::get('/', function () {
    return view('api');
});

Route::get('/api', function () {
    return view('api');
})->name('api');

Route::post('/api/list', [GitlabApiController::class, 'getContainerRegistryList'])->name('api-list');

Route::get('/api/view/{id}', [GitlabApiController::class, 'getContainerRegistryTag'])->name('tag-view');

**GITLAB API instuctions**

1. You need to generate PRIVATE-TOKEN in your gitlab account
2. Get project_id

**API with CI/CD project variables**
List project variables:
`curl --header "PRIVATE-TOKEN: ih_SBGzWT6sn4bXD38j9" "https://gitlab.com/api/v4/projects/26001527/variables"`

Show variable details:
`curl --header "PRIVATE-TOKEN: ih_SBGzWT6sn4bXD38j9" "https://gitlab.com/api/v4/projects/26001527/variables/SSH_WINGAMES_USER"`

Create variable:
`curl --request POST --header "PRIVATE-TOKEN: ih_SBGzWT6sn4bXD38j9" \
"https://gitlab.com/api/v4/projects/26001527/variables" --form "key=TEST_VAR" --form "value=test value1"`

Update variable:
`curl --request PUT --header "PRIVATE-TOKEN: ih_SBGzWT6sn4bXD38j9" \
"https://gitlab.com/api/v4/projects/26001527/variables/TEST_VAR" --form "value=updated VALUE"`

Remove variable:
`curl --request DELETE --header "PRIVATE-TOKEN: ih_SBGzWT6sn4bXD38j9" "https://gitlab.com/api/v4/projects/26001527/variables/TEST_VAR"`

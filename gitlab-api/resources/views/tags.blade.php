@extends('layout')

@section('main_content')
    @if(!empty($repositoryTag))
        <div class="container">
            <div class="text-center fw-bolder">
                {{ $repositoryTag['id'] }}
                {{ $repositoryTag['name'] }}
                {{ $repositoryTag['path'] }}
                {{ $repositoryTag['project_id'] }}
                {{ $repositoryTag['location'] }}
                {{ $repositoryTag['created_at'] }}
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">name</th>
                    <th scope="col">path</th>
                    <th scope="col">location</th>
                </tr>
                </thead>
                <tbody>
                @foreach($repositoryTag['tags'] as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $value['name'] }}</td>
                        <td>{{ $value['path'] }}</td>
                        <td>{{ $value['location'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <a href="/" class="btn btn-primary stretched-link">Home</a>
    @endif

@endsection

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Test Gitlab Api</title>
</head>
<body>
<div class="container">
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('api-list') }}" method="post">
        @csrf
        <select class="form-select form-select-sm" name="function">
            <option value="list_repository">List registry repositories</option>
            <option value="list_tags">Get details of a single repository</option>
            <option value="get_details">Get details of a group</option>
            <option value="get_details">List registry repository tags</option>
            <option value="get_details">Delete registry repository tags in bulk</option>
        </select>
        <select class="form-select form-select-sm" name="repository">
            <option value="7321468">Travellizy-api</option>
            <option value="7324389">Travellizy-client</option>
            <option value="12779035">Travellizy-integration</option>
            <option value="21928690">Travellizy-partners</option>
            <option value="7188051">Travellizy-personal-accaunt</option>
            <option value="14947845">Travellizy-flight-search-service</option>
            <option value="7238752">Travellizy-control</option>
            <option value="16498450">Travellizy-DevTeam</option>
        </select>
        <button type="submit" class="btn btn-primary">Submit</button>

        @yield('main_content')

    </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</div>
</body>
</html>

@extends('layout')

@section('main_content')
    @if(!empty($repository))
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">path</th>
                <th scope="col">project_id</th>
                <th scope="col">location</th>
                <th scope="col">created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach($repository as $key => $value)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td><a href="view/{{ $value['id'] }}">{{ $value['id'] }}</a></td>
                    <td>{{ $value['name'] }}</td>
                    <td>{{ $value['path'] }}</td>
                    <td>{{ $value['project_id'] }}</td>
                    <td>{{ $value['location'] }}</td>
                    <td>{{ $value['created_at'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

@endsection

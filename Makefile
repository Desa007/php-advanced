docker_compose = docker-compose -f ./docker/docker-compose.yml

#USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose up -d --build
start:
	$(docker_compose) up -d --build

stop:
	$(docker_compose) down

restart:
	make stop
	make start

composer-dump:
	$(docker_compose) exec php sh -c "composer dump-autoload"

composer-update:
	$(docker_compose) exec php sh -c "composer update"

root:
	$(docker_compose) exec --user=root php bash

exec :
	$(docker_compose) exec php bash

script :
	$(docker_compose) exec php php script.php
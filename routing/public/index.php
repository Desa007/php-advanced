<?php

require_once '../vendor/autoload.php';

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);
$router = new \Illuminate\Routing\Router($dispatcher, $container);

$router->get('/', function (){
    require_once 'html/index.html';
});

$router->get('/blog', function (){
    require_once 'html/services.html';
});

$router->get('/team', function (){
    require_once 'html/team.html';
});

$router->get('/contact-us', function (){
    require_once 'html/contact.html';
});
$request = \Illuminate\Http\Request::createFromGlobals();
$router->dispatch($request);

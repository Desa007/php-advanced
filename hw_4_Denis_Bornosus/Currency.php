<?php

declare(strict_types=1);

/**
 * Class Currency
 */
class Currency
{
    private $isoCode;

    /**
     * Currency constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        if (mb_strlen($value) !== 3 || ctype_upper($value) === false) {
            throw new InvalidArgumentException('Currency input must be ISO 4217');
        }

        $this->isoCode = $value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->isoCode;
    }

    /**
     * @param Currency $currency
     * @return bool
     */
    public function equals(self $currency): bool
    {
//        return (string)$this === (string)$currency;
        return $this->isoCode === $currency->isoCode;
    }
}
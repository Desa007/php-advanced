<?php
declare(strict_types=1);

require_once 'Currency.php';
require_once 'Money.php';

$currency1 = new Currency('USD');
$money1 = new Money(1700001, new Currency('USD'));

//echo (string)$currency1;

try {
//    $currency2 = new Currency('USD');
//    echo $currency1->equals($currency2) ? 'Your currencies are equal' : 'Your currencies NOT equal'; //сравнение вводимых валют

    $money2 = new Money(107002, new Currency('USD'));
    if ($money1->equals($money2)){
        echo 'Your currencies are equal';
    }else{
        throw new InvalidArgumentException('Your currencies NOT equal');
    }
//    echo $money1->equals($money2) ? 'Your currencies are equal' : 'Your currencies NOT equal'; // сравнение по числу и валюте
    echo $money1->add($money2) . ' ' . $money1->getCurrency(); // суммирование валюты
} catch (Exception $exception){
    echo $exception->getMessage();
}


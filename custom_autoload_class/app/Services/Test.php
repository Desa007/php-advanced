<?php


namespace App\Services;


class Test
{
    public $var;

    /**
     * @return mixed
     */
    public function getVar()
    {
        return $this->var;
    }

    /**
     * @param mixed $var
     */
    public function setVar($var): void
    {
        $this->var = $var;
    }

}
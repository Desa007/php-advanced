

<?php $__env->startSection('content'); ?>
    <section class="page_breadcrumbs ds background_cover background_overlay section_padding_top_65 section_padding_bottom_65">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="highlight">Team type 1</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="ls page_portfolio section_padding_top_150 section_padding_bottom_130">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="isotope_container isotope row masonry-layout columns_margin_bottom_20">

                        <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teamItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="isotope-item col-lg-4 col-md-6 col-sm-12">
                                <div class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
                                    <div class="item-media">
                                        <img src="<?php echo e($teamItem->photo); ?>" alt="" />
                                    </div>
                                    <div class="item-content">
                                        <p class="small-text highlight3 bottommargin_0"><?php echo e($teamItem->position); ?></p>
                                        <h4 class="topmargin_0 hover-color3">
                                            <a href="team-single.html"><?php echo e($teamItem->first_name); ?> <?php echo e($teamItem->last_name); ?></a>
                                        </h4>
                                        <p>
                                            <?php echo e($teamItem->description_sm); ?>

                                        </p>
                                        <p class="color2links">
                                            <a href="<?php echo e($teamItem->facebook); ?>" class="social-icon soc-facebook"></a>
                                            <a href="<?php echo e($teamItem->twitter); ?>" class="social-icon soc-twitter"></a>
                                            <a href="<?php echo e($teamItem->google_plus); ?>" class="social-icon soc-google"></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                    <!-- eof .isotope_container.row -->

                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /src/app/resources/views/team.blade.php ENDPATH**/ ?>
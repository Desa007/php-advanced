<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('teams', function ($table){
    $table->bigIncrements('id');
    $table->string('photo', 255);
    $table->string('position', 100);
    $table->string('first_name', 100);
    $table->string('last_name', 100);
    $table->string('description_sm', 500);
    $table->string('facebook', 1000);
    $table->string('twitter', 1000);
    $table->string('google_plus', 1000);
    $table->timestamps();
});

$data = [
    [
        'photo' => 'images/team/01.jpg',
        'position' => 'director',
        'first_name' => 'Theresa',
        'last_name' => 'Green',
        'description_sm' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'https://www.facebook.com/',
        'twitter' => 'https://twitter.com/?lang=ru',
        'google_plus' => 'https://www.google.com/',
    ],
    [
        'photo' => 'images/team/02.jpg',
        'position' => 'cat groomer',
        'first_name' => 'Rosie',
        'last_name' => 'White',
        'description_sm' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'https://www.facebook.com/',
        'twitter' => 'https://twitter.com/?lang=ru',
        'google_plus' => 'https://www.google.com/',
    ],
    [
        'photo' => 'images/team/03.jpg',
        'position' => 'dog groomer',
        'first_name' => 'Estelle',
        'last_name' => 'Marsh',
        'description_sm' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'https://www.facebook.com/',
        'twitter' => 'https://twitter.com/?lang=ru',
        'google_plus' => 'https://www.google.com/',
    ],
    [
        'photo' => 'images/team/04.jpg',
        'position' => 'rabbit groomer',
        'first_name' => 'Alberta',
        'last_name' => 'Rogers',
        'description_sm' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'https://www.facebook.com/',
        'twitter' => 'https://twitter.com/?lang=ru',
        'google_plus' => 'https://www.google.com/',
    ],
    [
        'photo' => 'images/team/05.jpg',
        'position' => 'cat groomer',
        'first_name' => 'Bernard',
        'last_name' => 'Lambert',
        'description_sm' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'https://www.facebook.com/',
        'twitter' => 'https://twitter.com/?lang=ru',
        'google_plus' => 'https://www.google.com/',
    ],
    [
        'photo' => 'images/team/06.jpg',
        'position' => 'rabbit groomer',
        'first_name' => 'Frank',
        'last_name' => 'Stephens',
        'description_sm' => 'Cow ham hock sausage ribeye fatback. Pig sausage turducken, beef drumstick corned beef andouille.',
        'facebook' => 'https://www.facebook.com/',
        'twitter' => 'https://twitter.com/?lang=ru',
        'google_plus' => 'https://www.google.com/',
    ],
];

foreach ($data as $nav) {
    $model = new \App\Model\Team();
    $model->photo = $nav['photo'];
    $model->position = $nav['position'];
    $model->first_name = $nav['first_name'];
    $model->last_name = $nav['last_name'];
    $model->description_sm = $nav['description_sm'];
    $model->facebook = $nav['facebook'];
    $model->twitter = $nav['twitter'];
    $model->google_plus = $nav['google_plus'];
    $model->save();
}
<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('group_student', function ($table){
    //BIGINT equivalent column.
    $table->unsignedBigInteger('group_id');
    //BIGINT equivalent column.
    $table->unsignedBigInteger('student_id');
    $table->timestamps();
    $table->softDeletes();
    $table->foreign('group_id')->references('id')->on('groups');
    $table->foreign('student_id')->references('id')->on('students');
});
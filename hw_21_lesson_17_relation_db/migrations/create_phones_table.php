<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('phones', function ($table){
    $table->bigIncrements('id');
    $table->unsignedBigInteger('user_id');
    $table->string('phone', 255);
    $table->timestamps();

    $table->foreign('user_id')->references('id')->on('users');
});

$data = [
    [
        'user_id' => 1,
        'phone' => '+380670000000',
    ],
];

foreach ($data as $phone) {
    $model = new \App\Model\Phone();
    $model->user_id = $phone['user_id'];
    $model->phone = $phone['phone'];
    $model->save();
}
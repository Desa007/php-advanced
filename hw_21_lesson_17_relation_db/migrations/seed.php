<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

// Courses
$data = [
    ['title' => 'PHP Basic', 'amount' => '16'],
    ['title' => 'PHP ADVANCED', 'amount' => '32'],
];
foreach ($data as $row) {
    $model = new \App\Model\Course();
    $model->title = $row['title'];
    $model->amount = $row['amount'];
    $model->save();
}

// Groups
$data = [
    ['course_id' => '1', 'start_date' => '2020-01-01'],
    ['course_id' => '2', 'start_date' => '2020-01-20'],
];
foreach ($data as $row) {
    $model = new \App\Model\Group();
    $model->course_id = $row['course_id'];
    $model->start_date = $row['start_date'];
    $model->save();
}

// Lessons
$data = [
    ['group_id' => '1', 'title' => 'lesson1', 'description' => 'lesson1 for group1', 'start_date' => '2020-01-01'],
    ['group_id' => '2', 'title' => 'lesson1', 'description' => 'lesson1 for group2', 'start_date' => '2020-01-01'],
];
foreach ($data as $row) {
    $model = new \App\Model\Lesson();
    $model->group_id = $row['group_id'];
    $model->title = $row['title'];
    $model->description = $row['description'];
    $model->start_date = $row['start_date'];
    $model->save();
}

// Students
$data = [
    ['first_name' => 'Ivan', 'last_name' => 'Ivanov', 'birthday' => '1987-10-01'],
    ['first_name' => 'Petr', 'last_name' => 'Petrov', 'birthday' => '1987-11-02'],
    ['first_name' => 'Igor', 'last_name' => 'Igorev', 'birthday' => '1987-12-03'],
];
foreach ($data as $row) {
    $model = new \App\Model\Student();
    $model->first_name = $row['first_name'];
    $model->last_name = $row['last_name'];
    $model->birthday = $row['birthday'];
    $model->save();
}

// Group_student
$data = [
    ['group_id' => '1', 'student_id' => '1'],
    ['group_id' => '2', 'student_id' => '2'],
    ['group_id' => '2', 'student_id' => '3'],
];
foreach ($data as $row) {
    $model = new \App\Model\Group_student();
    $model->group_id = $row['group_id'];
    $model->student_id = $row['student_id'];
    $model->save();
}
<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('courses', function ($table){
    //Auto-incrementing UNSIGNED BIGINT (primary key) equivalent column.
    $table->bigIncrements('id');
    //VARCHAR equivalent column with a length.
    $table->string('title', 255);
    //UNSIGNED TINYINT equivalent column.
    $table->unsignedTinyInteger('amount');
    //Adds nullable created_at and updated_at TIMESTAMP equivalent columns with precision (total digits).
    $table->timestamps();
    //Adds a nullable deleted_at TIMESTAMP equivalent column for soft deletes with precision (total digits).
    $table->softDeletes();
});
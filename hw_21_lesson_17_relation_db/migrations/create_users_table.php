<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('users', function ($table){
    $table->bigIncrements('id');
    $table->string('email', 255);
    $table->string('password', 255);
    $table->timestamps();
});

$data = [
    [
        'id' => 1,
        'email' => 'some@gmail.com',
        'password' => password_hash('password', PASSWORD_BCRYPT)
    ],
];

foreach ($data as $user) {
    $model = new \App\Model\User();
    $model->id = $user['id'];
    $model->email = $user['email'];
    $model->password = $user['password'];
    $model->save();
}
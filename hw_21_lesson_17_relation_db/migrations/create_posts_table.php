<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('posts', function ($table){
    $table->bigIncrements('id');
    $table->unsignedBigInteger('user_id');
    $table->string('title', 255);
    $table->timestamps();

    $table->foreign('user_id')->references('id')->on('users');
});

$data = [
    ['user_id' => 1, 'title' => 'Titile 1',],
    ['user_id' => 1, 'title' => 'Titile 2',],
];

foreach ($data as $phone) {
    $model = new \App\Model\Post();
    $model->user_id = $phone['user_id'];
    $model->title = $phone['title'];
    $model->save();
}
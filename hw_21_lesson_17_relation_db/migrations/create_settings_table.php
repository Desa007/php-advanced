<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('settings', function ($table){
    $table->bigIncrements('id');
    $table->string('title', 100);
    $table->string('value', 100);
    $table->timestamps();
});

$data = [
    ['value' => '8 800 269 8469', 'title' => 'phone',],
    ['value' => '253 Adams Ave, Iowa', 'title' => 'location',],
    ['value' => 'Mon - Sat 8am - 6pm', 'title' => 'open hours',],
    ['value' => 'https://www.facebook.com/', 'title' => 'facebook',],
    ['value' => 'https://twitter.com/?lang=ru', 'title' => 'twitter',],
    ['value' => 'https://google.com', 'title' => 'googl plus',],
];

foreach ($data as $nav) {
    $model = new \App\Model\Settings();
    $model->title = $nav['title'];
    $model->value = $nav['value'];
    $model->save();
}
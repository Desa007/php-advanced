<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Lesson extends Model
{
    use SoftDeletes;

    public function group()
    {
        // inverse One To One
        return $this->belongsTo(Group::class);
    }
}
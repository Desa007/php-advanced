<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Group_student extends Model
{
    use SoftDeletes;
    protected $table = 'group_student';
}
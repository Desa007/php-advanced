<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Group extends Model
{
    use SoftDeletes;

    public function course()
    {
        // inverse One To One
        return $this->belongsTo(Course::class);
    }

    public function students()
    {
        // Many-to-many
        return $this->belongsToMany(Student::class);
    }

    public function lessons()
    {
        // One To Many
        return $this->hasMany(Group::class);
    }
}
<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

final class Post extends Model
{
    public function user()
    {
        return $this->belongsTo(\App\Model\User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(\App\Model\Tag::class);
    }
}

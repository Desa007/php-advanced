<?php

$engineResolver = new \Illuminate\View\Engines\EngineResolver();

$fileSystem = new \Illuminate\Filesystem\Filesystem();
$fileViewFinder = new \Illuminate\View\FileViewFinder(
    $fileSystem, ['../resources/views']
);

$compiler = new \Illuminate\View\Compilers\BladeCompiler($fileSystem, '../resources/cache');

$engineResolver->register('blade', function () use ($compiler) {
    return new \Illuminate\View\Engines\CompilerEngine($compiler);
});

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);

$blade = new \Illuminate\View\Factory(
    $engineResolver,
    $fileViewFinder,
    $dispatcher
);

$blade->composer('layouts.layout', function($views){
    $views->with('nav', \App\Model\Navigation::all());
});



$blade->composer('layouts.layout', function($views){
    $settings = \App\Model\Settings::all();
    $params = [];
    foreach($settings as $setting) {
        $params[$setting->title] = $setting->value;
    }
    $views->with('settings', $params);
});
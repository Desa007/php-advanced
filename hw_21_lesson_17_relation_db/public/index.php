<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';
require_once '../config/blade.php';

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);
$router = new \Illuminate\Routing\Router($dispatcher, $container);

function view($view, array $data = [])
{
    global $blade;

    return $blade->make($view, $data)->render();
}

function router()
{
    global $router;
    return $router;
}

$router->get('/', \App\Controller\HomeController::class)->name('home');
$router->get('/blog', \App\Controller\BlogController::class)->name('blog');
$router->get('/services', \App\Controller\ServicesController::class)->name('services');
$router->get('/team', \App\Controller\TeamController::class)->name('team');
$router->get('/contact-us', \App\Controller\ContactController::class)->name('contacts');
$router->get('/tramp', \App\Controller\ContactController::class)->name('tramp');

$router->get('/one-to-one', function(){
    $user = \App\Model\User::find(1);
    //var_dump($user);
    //var_dump($user->phone());
    $phone = $user->phone;
    echo $phone->phone;
});

$router->get('/one-to-many', function(){
    $user = \App\Model\User::find(1);
    $posts = $user->posts;
//    var_dump($posts);
//    foreach($posts as $post){
//        echo '<br>'; print_r($post);
//    }
});

$router->get('/one-to-many-inverse', function(){
    $post = \App\Model\Post::find(1);
    var_dump($post->user);
});

$router->get('/many-to-many', function(){
//    $post = \App\Model\Post::find(1);
//    var_dump($post->tags);

    $tag = \App\Model\Tag::find(2);
    var_dump($tag->posts);
});

$router->get('/test', function(){
    $courses = \App\Model\Course::all();
    foreach($courses as $course){
        echo '<h1>'.$course->title.'</h1>';
        echo '<h2>Группы</h2>';
        foreach($course->groups as $group) {
//        foreach($course->groups()->where()->get() as $group) {
//        foreach($course->groups()->latest()->get() as $group) {

            $students = $group->students;
            //echo "<p>{$course->title} от {$group->start_date} ({$group->students()->count()})</p>";
            echo "<p>{$course->title} от {$group->start_date} ({$students->count()})</p>";

            foreach($students as $student) {
//            foreach($group->students as $student) {
                echo "<h4>{$student->first_name} {$student->last_name}</h4>";
            }
        }
    }
});

$request = \Illuminate\Http\Request::createFromGlobals();

$response = $router->dispatch($request);

echo $response->getContent();
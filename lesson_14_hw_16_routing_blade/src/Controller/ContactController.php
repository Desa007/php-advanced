<?php


namespace App\Controller;


class ContactController
{
    public function __invoke()
    {
        echo view('contact');
    }
}
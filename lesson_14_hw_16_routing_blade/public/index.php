<?php

require_once '../vendor/autoload.php';
require_once '../config/blade.php';

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);
$router = new \Illuminate\Routing\Router($dispatcher, $container);

function view($view, array $data = [])
{
    global $blade;

    return $blade->make($view, $data)->render();
}

function router()
{
    global $router;
    return $router;
}

$router->get('/', \App\Controller\BaseController::class);
$router->get('/blog', \App\Controller\BlogController::class);
$router->get('/services', \App\Controller\ServicesController::class);
$router->get('/contact-us', \App\Controller\ContactController::class);
$router->get('/team', \App\Controller\TeamController::class);

$request = \Illuminate\Http\Request::createFromGlobals();
$router->dispatch($request);
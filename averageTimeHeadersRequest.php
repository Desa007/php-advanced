<?php

//$url = "https://api.travellizy.com";
//$url = "https://api.travellizy.com/test/index.php";
//$url = "http://api.gran.testizy.me";
$url = "https://api.github.com/graphql";
//$url = "http://116.203.197.73";
$iterations = 100;
$totalTime = 0;
$averageTime = 0;

function getUrlHeaders(string $url): array
{
    stream_context_set_default(
        [
            'http' => [
                'method' => 'HEAD'
            ]
        ]
    );

    return get_headers($url);
}

function getRequestHeadersTime (string $url): float
{
    $timeStart = microtime(true);
    getUrlHeaders($url);
    $timeEnd = microtime(true);

    return round($timeEnd - $timeStart, 5);
}

for ($i = 0; $i <= $iterations; $i++) {
    $totalTime += getRequestHeadersTime($url);
    sleep(1);
}
$averageTime = $totalTime / $iterations;

echo sprintf('Average time %f', $averageTime);
//echo "Average time $averageTime";



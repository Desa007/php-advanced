<?php
declare(strict_types=1);

require_once 'vendor/autoload.php';

use App\Services\Color;

$color1 = new Color(22, 22, 3);
$color2 = new Color(22, 221, 3);

var_dump($color1->mix($color2));
var_dump($color1->equals($color2));
var_dump(Color::random());

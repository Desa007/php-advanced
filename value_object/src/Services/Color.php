<?php
declare(strict_types=1);


namespace App\Services;

/**
 * Class Color
 * @package App\Services
 */
class Color
{
    private $red;
    private $green;
    private $blue;

    /**
     * Color constructor.
     * @param int $red
     * @param int $green
     * @param int $blue
     * @throws \Exception
     */
    public function __construct(int $red, int $green, int $blue)
    {
        $this->setRed($red);
        $this->setGreen($green);
        $this->setBlue($blue);
    }

    /**
     * @return int
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * @return int
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * @return int
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * @param int $red
     * @throws \Exception
     */
    private function setRed(int $red): void
    {
        $this->checkDiapason($red);
        $this->red = $red;
    }

    /**
     * @param mixed $green
     * @throws \Exception
     */
    private function setGreen(int $green): void
    {
        $this->checkDiapason($green);
        $this->green = $green;
    }

    /**
     * @param mixed $blue
     * @throws \Exception
     */
    private function setBlue(int $blue): void
    {
        $this->checkDiapason($blue);
        $this->blue = $blue;
    }

    private function checkDiapason(int $val): bool
    {
        if ($val < 0 || $val > 255) {
            throw new \Exception('Wrong parametr range');
        }
        return true;
    }

    /**
     * @param Color $color
     * @return bool
     */
    public function equals(self $color): bool
    {
        return $this == $color;
//        return $this->blue === $color->blue && $this->red === $color->red && $this->green === $color->green;
    }

    /**
     * @return static
     */
    public static function random(): self
    {
        return new Color(rand(0, 255), rand(0, 255), rand(0, 255));
    }

    /**
     * @param Color $color
     * @return $this
     */
    public function mix(self $color): self
    {
        $red = (int)round(($color->getRed() + $this->red) / 2);
        $green = (int)round(($color->getGreen() + $this->green) / 2);
        $blue = (int)round(($color->getBlue() + $this->blue) / 2);

        return new Color($red, $green, $blue);
    }

}
